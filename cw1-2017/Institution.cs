﻿#region Summary

/*
 * Author: 
 * Asparuh Kamenov
 * 
 * Description:
 * The class which contains two properties for storing the name and address of an institution.
 * 
 * Date created:
 * 04/10/2016
*/

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw1_2017
{
    public class Institution
    {
        private string institutionName;
        private string institutionAddress;

        #region Properties

        /// <summary>
        /// Gets or sets the name of an institution.
        /// </summary>
        public string InstitutionName
        {
            get { return institutionName; }
            set { institutionName = value; }
        }

        /// <summary>
        /// Gets or sets the address of an institution.
        /// </summary>
        public string InstitutionAddress
        {
            get { return institutionAddress; }
            set { institutionAddress = value; }
        }
        #endregion
    }
}
