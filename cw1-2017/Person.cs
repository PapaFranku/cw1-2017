﻿#region Summary

/*
 * Author: 
 * Asparuh Kamenov
 * 
 * Description:
 * An abstract class which contains two properties for storing first and last names.
 * 
 * Date created:
 * 04/10/2016
*/

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw1_2017
{
    public abstract class Person
    {
        private string firstName;
        private string secondName;

        #region Properties
        /// <summary>
        /// Gets or sets the first name of a person, value cannot be blank.
        /// </summary>
        public string FirstName
        {
            get { return firstName; }
            set 
            { 
                //Check if the value is not blank
                if(value != "" )
                {
                    firstName = value;
                }
                else
                {
                    throw new FormatException("Name field cannot be empty!");
                }
            }
        }

        /// <summary>
        /// Gets or sets the first name of a person, value cannot be blank.
        /// </summary>
        public string SecondName
        {
            get { return secondName; }
            set
            {
                //Check if the value is not blank
                if (value != "")
                {
                    secondName = value;
                }
                else
                {
                    throw new FormatException("Name field cannot be empty!");
                }
            }
        }
        #endregion
    }
}
