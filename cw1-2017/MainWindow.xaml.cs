﻿#region Summary

/*
 * Author: 
 * Asparuh Kamenov
 * 
 * Description:
 * The main window of the program. It has text and combo boxes for providing information
 * about an attendee at a conference, as well as 5 buttons. It uses the attendee class for that purpose.
 * The set button writes the information provided in the attendee object. The clear button deletes all information
 * from the text boxes and resets the combo boxes to their default value. The get button gets the information,
 * previously stored in the attendee object and outputs it to the corresponding fields. The invoice button opens 
 * a new window containing a text box with information. The certificate button opens a new window containing a
 * text box, which represents a certificate stating that the attendee has attended the given conference.
 * 
 * Date created:
 * 04/10/2016
*/

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace cw1_2017
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Attendee attendee;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void AttendeeDetails_Main_Loaded(object sender, RoutedEventArgs e)
        {
            //Add the registration types in the combo box
            foreach(string s in Attendee.RegistrationTypes)
            {
                cboxRegType.Items.Add(s);
            }
            //Add yes and no options to the combo boxes for the bool properties
            cboxPaid.Items.Add("No");
            cboxPaid.Items.Add("Yes");           
            cboxPresenter.Items.Add("No");
            cboxPresenter.Items.Add("Yes");
            //Set up a default value
            cboxRegType.SelectedIndex = 0;
            cboxPaid.SelectedIndex = 0;
            cboxPresenter.SelectedIndex = 0;
            
            attendee = new Attendee();
        }

        private void cboxPresenter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //If the presenter box current selection is "Yes", show the paper title text box
            if(cboxPresenter.SelectedIndex == 1)
            {
                lblPaperTitle.Visibility = Visibility.Visible;
                txtBoxPaperTitle.Visibility = Visibility.Visible;
            }
            else
            {
                lblPaperTitle.Visibility = Visibility.Hidden;
                txtBoxPaperTitle.Visibility = Visibility.Hidden;
            }
        }

        #region Button Click Events
        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Set attendee properties with the values provided in the window
                attendee.FirstName = txtBoxFstName.Text;
                attendee.SecondName = txtBoxSndName.Text;

                //Check if the value in the text box for the reference number of the attendee
                //contains numbers only. The valid boundaries check is handled by the Attendee class.
                uint result;
                bool convert = uint.TryParse(txtBoxReference.Text, out result);
                if(convert == true)
                {
                    attendee.AttendeeRef = result;
                }
                else
                {
                    throw new InvalidCastException("Input is not numeric!");
                }

                attendee.AttendeeInstitution.InstitutionName = txtBoxInstitutionName.Text;
                attendee.AttendeeInstitution.InstitutionAddress = txtBoxInstitutionAddress.Text;
                attendee.ConferenceName = txtBoxConferenceName.Text;
                attendee.RegistrationType = cboxRegType.Text;

                //Set the bool properties of attendee,
                //depending on the current value of the combo boxes
                if(cboxPaid.Text == "Yes")
                {
                    attendee.Paid = true;
                }
                else
                {
                    attendee.Paid = false;
                }

                if(cboxPresenter.Text == "Yes")
                {
                    attendee.IsPresenter = true;
                }
                else
                {
                    attendee.IsPresenter = false;
                }

                //Set the attendee paper title property only if presenter combo box is "Yes"
                if(attendee.IsPresenter == true)
                {
                    attendee.PaperTitle = txtBoxPaperTitle.Text;
                }
            }
            catch(Exception excep)
            {
                MessageBox.Show(excep.Message, "Error");
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            //Get the collection of text boxes in the grid
            IEnumerable<TextBox> textBoxes = gridMain.Children.OfType<TextBox>();
            //Clear each one of them
            foreach(TextBox element in textBoxes)
            {
                element.Clear();
            }

            //Get the collection of combo boxes in the grid
            IEnumerable<ComboBox> comboBoxes = gridMain.Children.OfType<ComboBox>();
            //Set the index of each one to 0(default value)
            foreach(ComboBox element in comboBoxes)
            {
                element.SelectedIndex = 0;
            }
        }

        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            //Set the text and combo boxes to the values, provided from
            // the attendee property
            txtBoxFstName.Text = attendee.FirstName;
            txtBoxSndName.Text = attendee.SecondName;

            if (attendee.AttendeeRef != 0)
            {
                txtBoxReference.Text = attendee.AttendeeRef.ToString();
            }

            txtBoxInstitutionName.Text = attendee.AttendeeInstitution.InstitutionName;
            txtBoxInstitutionAddress.Text = attendee.AttendeeInstitution.InstitutionAddress;
            txtBoxConferenceName.Text = attendee.ConferenceName;

            //Loop through the items in the box with registration types
            for(int i = 0; i < cboxRegType.Items.Count; i++)
            {
                //If the current value corresponds the one held by the attendee object
                //make it the current value
                if((string)cboxRegType.Items[i] == attendee.RegistrationType)
                {
                    cboxRegType.SelectedIndex = i;
                }
            }

            //Set the current index, based on the bool values in the attendee object
            if(attendee.Paid == true)
            {
                cboxPaid.SelectedIndex = 1;
            }
            else
            {
                cboxPaid.SelectedIndex = 0;
            }

            if (attendee.IsPresenter == true)
            {
                cboxPresenter.SelectedIndex = 1;
                txtBoxPaperTitle.Text = attendee.PaperTitle;
            }
            else
            {
                cboxPresenter.SelectedIndex = 0;
            }
        }

        private void btnInvoice_Click(object sender, RoutedEventArgs e)
        {
            //Put both name properties in one string
            string names = attendee.FirstName + " " + attendee.SecondName;

            //Initialize the invoice window with the necessary values
            InvoiceWindow invoice = new InvoiceWindow(names, attendee.AttendeeInstitution.InstitutionName,
                attendee.ConferenceName, attendee.GetCost());

            invoice.ShowDialog();
        }

        private void btnCertificate_Click(object sender, RoutedEventArgs e)
        {
            //Put both name properties in one string
            string names = attendee.FirstName + " " + attendee.SecondName;

            //If the presenter property is true, send the paper title value as well
            if(attendee.IsPresenter == true)
            {
                CertificateWindow certificate = new CertificateWindow(names, attendee.ConferenceName,
                    attendee.PaperTitle);

                certificate.ShowDialog();
            }
            else
            {
                CertificateWindow certificate = new CertificateWindow(names, attendee.ConferenceName);

                certificate.ShowDialog();
            }
        }
        #endregion
    }
}
