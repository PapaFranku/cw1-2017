﻿#region Summary

/*
 * Author: 
 * Asparuh Kamenov
 * 
 * Description:
 * A window opened by the main window, which represents the invoice
 * of an attendee.
 * 
 * Date created:
 * 04/10/2016
*/

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cw1_2017
{
    /// <summary>
    /// Interaction logic for InvoiceWindow.xaml
    /// </summary>
    public partial class InvoiceWindow : Window
    {
        public InvoiceWindow(string name, string institution, string conferenceName, int toPay)
        {
            InitializeComponent();
            //Set the text box to readonly
            txtBoxInvoice.IsReadOnly = true;

            //Append the lines to the string builder, 
            //using the provided values from the main window
            StringBuilder buildInvoice = new StringBuilder();
            buildInvoice.AppendLine("Name: " + name);
            buildInvoice.AppendLine("Institution: " + institution);
            buildInvoice.AppendLine("Conference name: " + conferenceName);
            buildInvoice.AppendLine("Total cost: $" + toPay);

            //Build the text box with the contents from the string builder
            for(int i = 0; i < buildInvoice.Length; i++)
            {
                txtBoxInvoice.Text += buildInvoice[i];
            }
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
