﻿#region Summary

/*
 * Author: 
 * Asparuh Kamenov
 * 
 * Description:
 * The class provides ways to store information about an attendee at a conference.
 * It inherits the first and last name properties from the clas Person.
 * It also has a method for calculating the price that should be paid by the attendee, 
 * depending on their registration type.
 * 
 * Date created:
 * 04/10/2016
*/

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw1_2017
{
    class Attendee : Person
    {
        private Institution attendeeInstitution;

        private uint attendeeRef;

        private string conferenceName;
        private string regType;
        private string paperTitle;

        private bool presenter;
        private bool paid;

        /// <summary>
        ///  A readonly array representing the possible registration types.
        /// </summary>
        public static readonly string[] RegistrationTypes = {"Full", "Student", "Organiser"};

        public Attendee()
        {
            attendeeInstitution = new Institution();
        }

        #region Properties
        /// <summary>
        /// Gets or sets the name or address for the institution of the attendee
        /// </summary>
        public Institution AttendeeInstitution
        {
            get { return attendeeInstitution; }
            set { attendeeInstitution = value; }
        }

        /// <summary>
        /// Gets or sets the attendee reference number
        /// </summary>
        public uint AttendeeRef
        {
            get { return attendeeRef; }
            set
            {
                // Check if the value is between the possible numbers
                if(value >= 40000 && value <= 60000)
                {
                    attendeeRef = value;
                }
                else
                {
                    throw new Exception("The reference number can only be between 40000 and 60000!");
                }
            }
        }    
    
        /// <summary>
        /// Gets or sets the name of the conference.
        /// </summary>
        public string ConferenceName
        {
            get { return conferenceName; }
            set { conferenceName = value; }
        }

        /// <summary>
        /// Gets or sets the registration type of the attendee.
        /// </summary>
        public string RegistrationType
        {
            get { return regType; }
            set
            {
                bool isSuccessful = false;
                //Look through the possible registration types
                foreach(string s in RegistrationTypes)
                {
                    if(s == value)
                    {
                        regType = value;
                        isSuccessful = true;
                        break;
                    }
                }
                //If no match was found
                if (isSuccessful == false)
                {
                    throw new ArgumentException("Invalid input for registration type!");
                }
            }
        }

        /// <summary>
        /// Gets or sets the paper title of attendee, provided that the IsPresenter property is true.
        /// </summary>
        public string PaperTitle
        {
            get { return paperTitle; }
            set
            {
                //Check if presenter is set to true
                if(presenter == true && value != "")
                {
                    paperTitle = value;
                }
                else if (value == "")
                {
                    throw new Exception("Paper title cannot be blank!");
                }
                else
                {
                    throw new Exception("Attendee must be a presenter in order to be assigned a paper title!");
                }
               
            }
        }

        /// <summary>
        /// Gets or sets a bool value representing whether the attendee is a presenter.
        /// </summary>
        public bool IsPresenter
        {
            get { return presenter; }
            set { presenter = value; }
        }

        /// <summary>
        /// Gets or sets a bool value representing whether the attendee has paid.
        /// </summary>
        public bool Paid
        {
            get { return paid; }
            set { paid = value; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Calculates the price to be paid by the attendee based on their registration type.
        /// </summary>
        /// <returns>
        /// int
        /// </returns>
        public int GetCost()
        {
            switch(regType)
            {
                case "Full":
                    //If IsPresenter property calculate with 10% discount.
                    if (presenter == true)
                    {
                        return (500 / 100) * 90;
                    }
                    else
                    { return 500; }
                case "Student":
                    //If IsPresenter property calculate with 10% discount.
                    if (presenter == true)
                    {
                        return (300 / 100) * 90;
                    }
                    else
                    { return 300; }
                default:
                    break;
            }
            //If the attendee is an organiser the cost is 0
            return 0;
        }
        #endregion
    }
}
