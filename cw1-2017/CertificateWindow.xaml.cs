﻿#region Summary

/*
 * Author: 
 * Asparuh Kamenov
 * 
 * Description:
 * A window opened by the main window, which represents the certificate
 * of an attendee.
 * 
 * Date created:
 * 04/10/2016
*/

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cw1_2017
{
    /// <summary>
    /// Interaction logic for CertificateWindow.xaml
    /// </summary>
    public partial class CertificateWindow : Window
    {
        StringBuilder buildCertificate;

        public CertificateWindow(string name, string conferenceName)
        {
            InitializeComponent();
            //Set the text box to readonly
            txtBoxCertificate.IsReadOnly = true;
            buildCertificate = new StringBuilder();

            //Append the lines to the string builder, 
            //using the provided values from the main window
            buildCertificate.Append("This is to certify that " + name + " attended " + conferenceName + ".");

            Print();
        }

        public CertificateWindow(string name, string conferenceName, string paperTitle)
        {
            InitializeComponent();
            //Set the text box to readonly
            txtBoxCertificate.IsReadOnly = true;
            buildCertificate = new StringBuilder();

            //Append the lines to the string builder, 
            //using the provided values from the main window
            buildCertificate.Append("This is to certify that " + name + " attended " + conferenceName + 
                " and presented a paper entitled " + paperTitle + ".");

            Print();
        }

        /// <summary>
        /// Builds the text box with the contents from the string builder
        /// </summary>
        private void Print()
        {
            //Put in a separate method to avoid repeating code
            for (int i = 0; i < buildCertificate.Length; i++)
            {
                txtBoxCertificate.Text += buildCertificate[i];
            }
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
